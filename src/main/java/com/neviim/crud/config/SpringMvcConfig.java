package com.neviim.crud.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class SpringMvcConfig extends WebMvcConfigurationSupport {
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(new String[]{"/css/**"}).addResourceLocations(new String[]{"classpath:/static/css/"});
		registry.addResourceHandler(new String[]{"/font/**"}).addResourceLocations(new String[]{"classpath:/static/font/"});
		registry.addResourceHandler(new String[]{"/img/**"}).addResourceLocations(new String[]{"classpath:/static/img/"});
		registry.addResourceHandler(new String[]{"/js/**"}).addResourceLocations(new String[]{"classpath:/static/js/"});
	}
}
